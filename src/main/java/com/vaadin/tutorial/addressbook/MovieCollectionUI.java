package com.vaadin.tutorial.addressbook;

import java.io.File;
import java.util.HashMap;

import com.mmc.movieapi.MovieAPI;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.data.Container.Filter;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.FileResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.AbstractTextField.TextChangeEventMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@Title("Moviecollection")
@Theme("reindeer")
public class MovieCollectionUI extends UI {

	/* User interface components are stored in session. */
	
	
	private MainComposite mainLayout = new MainComposite(); 
	private Button searchButton = mainLayout.getsearchButton();
	private Table movieList = mainLayout.getmovieList();
	private TextField searchField = mainLayout.getsearchField();
	private Button actionButton = mainLayout.getactionButton();
	private Embedded logo = mainLayout.getlogo();
	private VerticalLayout infoList = mainLayout.getinfoList(); 
	private Embedded poster = mainLayout.getposter(); 
	
	
	//Path to web app root
	private final String basepath = VaadinService.getCurrent()
            .getBaseDirectory().getAbsolutePath();
	
	
	//Local database that contains movies
	private static IndexedContainer moviesContainer = createMoviesContainer();

	/*
	 * After UI class is created, init() is executed. UI is built and set up here.
	 */
	protected void init(VaadinRequest request) {
		initLayout();
		initMovieList();
		//initSearch();
		initIMDbSearch();
		
		//Populate few sample films to the table
		fillSamples();
		
	}


	private void initLayout()
	{
		setContent(mainLayout);		
		logo.setSource(new FileResource(new File(basepath+"/WEB-INF/files/logo.jpg")));
		poster.setVisible(false);
		actionButton.setVisible(false);
	}
	
	private void initIMDbSearch()
	{
		searchButton.addClickListener(new ClickListener() {
		public void buttonClick(ClickEvent event) {
			
		String searchInput = searchField.getValue();
		addInfo(searchInput);
		}
		});
		
		searchField.setTextChangeEventMode(TextChangeEventMode.LAZY);
		searchField.addTextChangeListener(new TextChangeListener()
		{
			public void textChange(final TextChangeEvent event)
			{
				String searchInput = event.getText();	
				addInfo(searchInput);
			}
				
		});
		
	}
	
	//Shows imdbSearh results
	private void addInfo(String searchInput)
	{
		System.out.println(searchInput);
		System.out.println(MovieAPI.movie.getData().toString());		
		MovieAPI.SetMovie(searchInput);
		HashMap<String,String> datamap = MovieAPI.movie.getData();
		String[] fieldnames = MovieAPI.visibelFields;
		
		infoList.removeAllComponents();
		Label l = new Label(datamap.get("title"));
		l.setStyleName("h1");
		infoList.addComponent(l);
		for(int i = 1; i < fieldnames.length; i ++)
		{
			infoList.addComponent(new Label(fieldnames[i].toUpperCase()+": "+ datamap.get(fieldnames[i])));
		}
		Link trailerLink = new Link();
		trailerLink.setResource(new ExternalResource(datamap.get("trailer")));
		trailerLink.setCaption("Trailer");
		trailerLink.setStyleName("h3");
		infoList.addComponent(trailerLink);
		
		poster.setSource(new ExternalResource(datamap.get("poster")));
		poster.setVisible(true);
		
		actionButton.setCaption("Add");
		actionButton.setVisible(true);
		
		for(Object listener : actionButton.getListeners(ClickEvent.class)){
			      actionButton.removeListener(ClickEvent.class, listener);
			}
		
		actionButton.addClickListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {

				/*
				 * Rows in the Container data model are called Item. Here we add
				 * a new row in the beginning of the list.
				 */
				moviesContainer.removeAllContainerFilters();
				Object newMovie = moviesContainer.addItem();
				System.out.println(moviesContainer.getItemIds(0, 10));
				/*
				 * Each Item has a set of Properties that hold values. Here we
				 * set a couple of those.
				 */
				for(String fname : MovieAPI.movieFields)
				{
					moviesContainer.getContainerProperty(newMovie, fname).setValue(MovieAPI.movie.getData().get(fname));;
				}
				
				

				/* Lets choose the newly created movie to edit it. */
				movieList.select(newMovie);
			}
		});
		
		
	}

	private void initSearch() {

		/*
		 * We want to show a subtle prompt in the search field. We could also
		 * set a caption that would be shown above the field or description to
		 * be shown in a tooltip.
		 */
		searchField.setInputPrompt("Search movies");

		/*
		 * Granularity for sending events over the wire can be controlled. By
		 * default simple changes like writing a text in TextField are sent to
		 * server with the next Ajax call. You can set your component to be
		 * immediate to send the changes to server immediately after focus
		 * leaves the field. Here we choose to send the text over the wire as
		 * soon as user stops writing for a moment.
		 */
		searchField.setTextChangeEventMode(TextChangeEventMode.LAZY);

		/*
		 * When the event happens, we handle it in the anonymous inner class.
		 * You may choose to use separate controllers (in MVC) or presenters (in
		 * MVP) instead. In the end, the preferred application architecture is
		 * up to you.
		 */
		searchField.addTextChangeListener(new TextChangeListener() {
			public void textChange(final TextChangeEvent event) {

				/* Reset the filter for the movieContainer. */
				moviesContainer.removeAllContainerFilters();
				moviesContainer.addContainerFilter(new ContactFilter(event
						.getText()));
			}
		});
	}

	/*
	 * A custom filter for searching names and companies in the
	 * movieContainer.
	 */
	private class ContactFilter implements Filter {
		private String needle;

		public ContactFilter(String needle) {
			this.needle = needle.toLowerCase();
		}

		public boolean passesFilter(Object itemId, Item item) {
			String haystack = ("" + item.getItemProperty("title").getValue()
					+ item.getItemProperty("year").getValue() + item
					.getItemProperty("director").getValue()).toLowerCase();
			return haystack.contains(needle);
		}

		public boolean appliesToProperty(Object id) {
			return true;
		}
	}



	private void initMovieList() {
		movieList.setContainerDataSource(moviesContainer);
		movieList.setVisibleColumns(new String[] { "title", "year", "genre" });
		movieList.setSelectable(true);
		movieList.setImmediate(true);

		movieList.addValueChangeListener(new Property.ValueChangeListener()
		
		{
			public void valueChange(ValueChangeEvent event) {
				Object movieId = movieList.getValue();

				/*
				 * When a movie is selected from the list, we want to show
				 * that in our editor on the right. This is nicely done by the
				 * FieldGroup that binds all the fields to the corresponding
				 * Properties in our movie at once.
				 */
				
				if (movieId != null)
				{	
					infoList.removeAllComponents();
					String[] savedFields = moviesContainer.getContainerPropertyIds().toArray(new String[0]);
					Label l = new Label(moviesContainer.getContainerProperty(movieId, "title").getValue().toString());
					l.setStyleName("h1");
					infoList.addComponent(l);
					
					for(int i = 1; i <  MovieAPI.visibelFields.length; i ++)
					{
						infoList.addComponent(new Label(MovieAPI.visibelFields[i].toUpperCase()+": "+moviesContainer.getContainerProperty(movieId, MovieAPI.visibelFields[i]).getValue()));
					}
					
					Link trailerLink = new Link();
					trailerLink.setResource(new ExternalResource(moviesContainer.getContainerProperty(movieId, "trailer").getValue().toString()));
					infoList.addComponent(trailerLink);
					
					poster.setSource(new ExternalResource(moviesContainer.getContainerProperty(movieId, "poster").getValue().toString()));
					poster.setVisible(true);
					
					for(Object listener : actionButton.getListeners(ClickEvent.class)){
					      actionButton.removeListener(ClickEvent.class, listener);
					}
					//Set up Remove button
					actionButton.addClickListener(new ClickListener() {
						public void buttonClick(ClickEvent event) {
							Object movieId = movieList.getValue();
							movieList.removeItem(movieId);
							infoList.removeAllComponents();
							poster.setVisible(false);
							actionButton.setVisible(false);
						}
					});
					actionButton.setCaption("Remove");
					actionButton.setVisible(true);
					
						
					
				infoList.setVisible(movieId != null);
				}
			}
		});
	}

	/*
	 * Generate some in-memory example data to play with. In a real application
	 * we could be using SQLContainer, JPAContainer or some other to persist the
	 * data.
	 */
	
	
	
	private static IndexedContainer createMoviesContainer()
	{
		MovieAPI.SetMovie("No Country for Old Men");
		IndexedContainer mc = new IndexedContainer();
		
		
		for(String fname : MovieAPI.movieFields)
		{
			mc.addContainerProperty(fname, String.class, "");
		}
		
		return mc;
	}
	
	private static void fillSamples()
	{
		Object sampleMovie = moviesContainer.addItem();
		MovieAPI.SetMovie("No Country for Old Men");
		
		for(String fname : MovieAPI.movieFields)
		{
			moviesContainer.getContainerProperty(sampleMovie, fname).setValue(MovieAPI.movie.getData().get(fname));;
		}
	}
}
