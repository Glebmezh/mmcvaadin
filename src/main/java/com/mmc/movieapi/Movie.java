package com.mmc.movieapi;

import java.util.HashMap;

public class Movie {
    private HashMap<String, String> data = new HashMap<String, String>();
    
    public Movie(String trailer,String title, String year,String rated,String released,String runtime,
    		String genre,String director,String writer,String actors,String plot,
    		String poster,String imdbRating,String imdbVotes,String imdbID)
    {
    	data.put("trailer",trailer);
    	data.put("title", title);
    	data.put("year", year);
    	data.put("rated", rated);
    	data.put("released", released);
    	data.put("runtime", runtime);
    	data.put("genre", genre);
    	data.put("director", director);
    	data.put("writer", writer);
    	data.put("actors", actors);
    	data.put("plot", plot);
    	data.put("poster", poster);
    	data.put("imdbRating", imdbRating);
    	data.put("imdbVotes", imdbVotes);
    	data.put("imdbID", imdbID);
    }
    
    public HashMap<String,String> getData()
    {
    	return data;
    }
    
    public String getTrailer()
    
    {
    	return data.get("trailer");
    }
}
