package com.mmc.movieapi;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.net.URL;

import org.json.JSONException;
import org.json.XML;

public class GetTrailer {

	public static String getTrailer(String title) {
		// TODO Auto-generated method stub
		String formatedTitle = formatTitleForTrailerInfo(title);
		String jsonString= "";
		try {
			jsonString =XML.toJSONObject(readUrl("http://simpleapi.traileraddict.com/"+formatedTitle+"/trailer")).toString();
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			return "no trailer";
			
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			System.out.println("second ex");
			e1.printStackTrace();
		}
		
		
			int start = jsonString.indexOf(":\"<iframe");
			int ends = jsonString.indexOf("iframe>");
			String newString = jsonString.substring(start+2, ends-3);
			String s = newString.replace("\\", "");
			return s;
		
	}
	
	private static String formatTitleForTrailerInfo(String title){
		String s = title.replace("+", "-");
		return s.toLowerCase();
	}
	
	private static String readUrl(String urlString) throws Exception {
		BufferedReader reader = null;
		try {
			URL url = new URL(urlString);
			reader = new BufferedReader(new InputStreamReader(url.openStream()));
			StringBuffer buffer = new StringBuffer();
			int read;
			char[] chars = new char[1024];
			while ((read = reader.read(chars)) != -1)
				buffer.append(chars, 0, read);

			return buffer.toString();
		} 
		catch(FileNotFoundException fnf) {return "";}
		finally {
			if (reader != null)
				reader.close();
		}
	}

}
