package com.mmc.movieapi;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

public class MovieAPI {
	public static Movie movie;
	static boolean b;
	public static String[] movieFields = {"trailer","title","year","rated","released",
		"runtime","genre","director","writer","actors","plot", "poster","imdbRating","imdbVotes","imdbID"};
	
	public static String[] visibelFields = {"title","year","rated","released",
		"runtime","genre","director","writer","actors","plot","imdbRating","imdbVotes"};
	
	public static void SetMovie(String title) {
		// TODO Auto-generated method stub
		String formatedTitle = formatTitleForMovieInfo(title);
		try {
		    JSONObject json = null;
			try {
				json = new JSONObject(readUrl("http://www.omdbapi.com/?i=&t="+formatedTitle));
			} catch (Exception e) {			
			
			}
			
			movie = new Movie(GetTrailer.getTrailer(formatedTitle), (String)json.get("Title"), (String)json.get("Year"), (String)json.get("Rated"),
					(String)json.get("Released"), (String)json.get("Runtime"), (String)json.get("Genre"),
					(String)json.get("Director"), (String)json.get("Writer"), (String)json.get("Actors"), (String)json.get("Plot"),
					(String)json.get("Poster"), (String)json.get("imdbRating"), (String)json.get("imdbVotes"), (String)json.get("imdbID"));
		    
			
			
		  

		} catch (JSONException e) {
			//if no movie
			movie = new Movie("n/a","n/a","n/a","n/a","n/a","n/a","n/a","n/a","n/a","n/a","n/a","n/a","n/a","n/a","n/a");
			

		}

	}
	
	
	private static String formatTitleForMovieInfo(String title){
		String s = title.replace(" ", "+");
		return s.toLowerCase();
	}

	private static String readUrl(String urlString) throws Exception {
		BufferedReader reader = null;
		try {
			URL url = new URL(urlString);
			reader = new BufferedReader(new InputStreamReader(url.openStream()));
			StringBuffer buffer = new StringBuffer();
			int read;
			char[] chars = new char[1024];
			while ((read = reader.read(chars)) != -1)
				buffer.append(chars, 0, read);

			return buffer.toString();
		} finally {
			if (reader != null)
				reader.close();
		}
	}
	public static void print(String s){
		System.out.println(s);
	}
}





